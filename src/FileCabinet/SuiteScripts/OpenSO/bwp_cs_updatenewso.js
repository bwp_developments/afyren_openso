var SEARCHMODULE, UIDIALOGMODULE, URLMODULE, RECORDMODULE;

/**
 * @NApiVersion 2.0
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */

// This script must apply on sales orders
define(['N/search',  'N/ui/dialog', 'N/url', 'N/record' ], runClient);

function runClient(search,  uidialog, url, record) {
	SEARCHMODULE= search;
	UIDIALOGMODULE= uidialog;
	URLMODULE= url;
	RECORDMODULE= record;
		
	var returnObj = {};
	returnObj['pageInit'] = _pageInit;
//	returnObj['fieldChanged'] = _fieldChanged;
//	returnObj['postSourcing'] = _postSourcing;
//	returnObj['sublistChanged'] = _sublistChanged;
//	returnObj['lineInit'] = _lineInit;
//	returnObj['validateField'] = _validateField;
//	returnObj['validateLine'] = _validateLine;
//	returnObj['validateInsert'] = _validateInsert;
//	returnObj['validateDelete'] = _validateDelete;
	returnObj['saveRecord'] = _saveRecord;

	return returnObj;
}
    
/**
 * Function to be executed after page is initialized.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
 *
 * @since 2015.2
 */
function _pageInit(scriptContext){
	var createdFromOpenSO = false;	
	var openSOId = '0';	
	var url = window.location.href;//get the URL of the page	
	var params = url.split('&'); //separate url and parameters	
	if (params.length > 1) {		
	 	var firstParam = params[1].split('=');
		if (firstParam && firstParam.length > 1 && firstParam[0] == 'custparam_bwp_openso') {
			openSOId = firstParam[1];
			createdFromOpenSO = true;				
			//Load Open sales order record
			var openSORecord = RECORDMODULE.load({
				type: 'customsale_bwp_open_sales_order',
				id: openSOId
			});
			updateHeaderSalesOrder(scriptContext, openSORecord);
			addLineToSalesOrder(scriptContext, openSORecord);
		}
	}
//	if (!createdFromOpenSO) {
//		return;
//	}
}


/**
 * Function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @since 2015.2
 */
function _fieldChanged(scriptContext) {
//	alert('field changed');
}

/**
 * Function to be executed when field is slaved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 *
 * @since 2015.2
 */
function _postSourcing(scriptContext) {

}

/**
 * Function to be executed after sublist is inserted, removed, or edited.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _sublistChanged(scriptContext) {

}

/**
 * Function to be executed after line is selected.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _lineInit(scriptContext) {

}

/**
 * Validation function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @returns {boolean} Return true if field is valid
 *
 * @since 2015.2
 */
function _validateField(scriptContext) {

}

/**
 * Validation function to be executed when sublist line is committed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateLine(scriptContext) {

}

/**
 * Validation function to be executed when sublist line is inserted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateInsert(scriptContext) {

}

/**
 * Validation function to be executed when record is deleted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateDelete(scriptContext) {

}

/**
 * Validation function to be executed when record is saved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @returns {boolean} Return true if record is valid
 *
 * @since 2015.2
 */

function _saveRecord(scriptContext) {
	
	var returnValue = verifyOpenSalesOrder(scriptContext);

	return returnValue;
}


function verifyOpenSalesOrder(scriptContext){	
		
	var returnValue = true;
	
	var openSOId = scriptContext.currentRecord.getValue('custbody_bwp_so_associated_oso');	
	if (openSOId) {		
		// Load Open sales order record
		var openSORecord = RECORDMODULE.load({
			type: 'customsale_bwp_open_sales_order',
			id: openSOId
		});		
    	var osoStartDate = openSORecord.getValue({
			fieldId: 'startdate',
		});
    	var osoEndDate = openSORecord.getValue({			 
			fieldId: 'enddate'
		});
    	
		// Load new sales order
		var soRecord = scriptContext.currentRecord;		
		var soTransactionDate = soRecord.getValue({ 
	    	 fieldId:'trandate',	    	
	    });
		
		if((osoStartDate <= soTransactionDate || !osoStartDate ) &&  (osoEndDate >= soTransactionDate || !osoEndDate)) {
    		
		} else {
			alert ("The transaction date doesn’t correspond to the effective date interval of the Open Sales Order")  		
			returnValue = false;			
		}
		
//    	SO Customer is linked with Open SO customer
    	var customerOSO = openSORecord.getValue('entity');
    	
    	var customerSO = soRecord.getValue({ 
	    	 fieldId:'entity',	    	
	    });    	
    	
    	if(customerSO != customerOSO) {    		
    		var customerSOParent = soRecord.getValue({ 
    			fieldId:'custbody_bwp_customer_parent',
   	    	 });
    		
    		// Parent customer management
    		var fieldsValues = SEARCHMODULE.lookupFields( {
    			type: SEARCHMODULE.Type.CUSTOMER,
    			id: customerSO,
    			columns: ['parent']
    		});
    		
    		var newCustomerSOParent;	
    		if ('parent' in fieldsValues && fieldsValues.parent.length > 0) {
    			newCustomerSOParent = fieldsValues.parent[0].value;
    		}    		
    		if(customerSOParent != newCustomerSOParent){
    			returnValue = false;
    			alert ("The customer is not related to the Open Sales Order customer");
    		}
		}		
			    
		// Go through all the new sales order lines
    	
		var maxSOLine = soRecord.getLineCount({sublistId: 'item'});
		for (var soLine = 0 ; soLine < maxSOLine ; soLine++){
			var soItem = soRecord.getSublistValue({ 
				sublistId: 'item', 
				fieldId:'item',
				line: soLine
			});    	    	
			var soItemDisplay = soRecord.getSublistValue({ 
				sublistId: 'item', 
				fieldId:'item_display',
				line: soLine
			});
			var soQuantity = soRecord.getSublistValue({ 
				sublistId: 'item',
				fieldId:'quantity',
				line: soLine
			});    	
			soQuantity = soQuantity || 0;
			
			var soReservedQuantity = soRecord.getSublistValue({ 
				sublistId: 'item',
				fieldId:'custcol_bwp_oso_reserved_quantity',
				line: soLine
			});    	
			soReservedQuantity = soReservedQuantity || 0;
			
			var soOrderedQuantity = soRecord.getSublistValue({ 
				sublistId: 'item',
				fieldId:'custcol_bwp_oso_ord_quantity',
				line: soLine
			});    	
			soOrderedQuantity = soOrderedQuantity || 0;
			
			var indexOSOItem = openSORecord.findSublistLineWithValue({
				sublistId: 'item', 
				fieldId: 'custcol_bwp_oso_so_item', 
				value: soItem
			});
	    	
			if (indexOSOItem < 0 ) {
				alert ("The item " + soItemDisplay + " is not present on the Open Sales Order.");
				returnValue = false;	    		
			} else {	    	
				// We get the sublist line item of the openSO	    	
				var osoOrderedQuantity = openSORecord.getSublistValue({
					sublistId: 'item', 
					fieldId: 'custcol_bwp_oso_ord_quantity',
					line: indexOSOItem
				});
				var osoReservedQuantity = openSORecord.getSublistValue({
					sublistId: 'item', 
					fieldId: 'custcol_bwp_oso_reserved_quantity',
					line: indexOSOItem
				});
				var osoOpenQuantity = openSORecord.getSublistValue({
					sublistId: 'item', 
					fieldId: 'custcol_bwp_oso_open_quantity',
					line: indexOSOItem
				});
				var osoQuantity = openSORecord.getSublistValue({
					sublistId: 'item', 
					fieldId: 'quantity',
					line: indexOSOItem
				});		    	

				// Verify the lines apply good rules
				var osoOpenQuantity = osoQuantity - osoReservedQuantity - osoOrderedQuantity ;		    	
				var osoNewOpenQuantity = osoOpenQuantity - soQuantity + soReservedQuantity + soOrderedQuantity;
				
				if (osoNewOpenQuantity < 0 ){
					alert ("Open Quantity for item " + soItemDisplay + " can't be greater than " + osoOpenQuantity + ".");
					returnValue = false;
				}				
			}	    	
		}	
	}
	return returnValue;
}

function updateHeaderSalesOrder(scriptContext, openSORecord){
	
	// Load Actual sales order
	var soRecord = scriptContext.currentRecord;
	
	// Set Value from OSO to SO
	soRecord.setValue({
		fieldId: 'entity',
		value: openSORecord.getValue('entity')
	});
	
	soRecord.setValue({
		fieldId: 'salesrep',
		value: openSORecord.getValue('salesrep')
	});
	
	soRecord.setValue({
		fieldId: 'subsidiary',
		value: openSORecord.getValue('subsidiary')
	});
	
	soRecord.setValue({
		fieldId: 'class',
		value: openSORecord.getValue('class')
		
	});
	
	soRecord.setValue({
		fieldId: 'location',
		value: openSORecord.getValue('location')
	});
	
	soRecord.setValue({
		fieldId: 'department',
		value: openSORecord.getValue('department')
	});
	
	soRecord.setValue({
		fieldId: 'custbody_bwp_so_associated_oso',
		value: openSORecord.getValue('id')
	});	
	
	// Parent customer management
	var openSOEntity = openSORecord.getValue('entity');
	
	var fieldsValues = SEARCHMODULE.lookupFields( {
		type: SEARCHMODULE.Type.CUSTOMER,
		id: openSOEntity,
		columns: ['parent']
	});
		
	var customerParent = openSOEntity;
	if ('parent' in fieldsValues && fieldsValues.parent.length > 0) {
		customerParent = fieldsValues.parent[0].value;
	}	
		
	soRecord.setValue({
		fieldId: 'custbody_bwp_customer_parent',
		value: customerParent
	});	
	
}

function addLineToSalesOrder(scriptContext, openSORecord) {
	//	Load current sales order record
    var soRecord = scriptContext.currentRecord;    
	//	Get open sales order id
	var openSOId = soRecord.getValue('custbody_bwp_so_associated_oso');
	
	if (!openSOId) {
		return;
	}
    
	// Get max line in the open sales order
    var maxOSOLine = openSORecord.getLineCount({
    	sublistId: 'item'
    });
    
    // Go through the open sales order line
    for (var osoLine = 0 ; osoLine < maxOSOLine ; osoLine++){    	
    	// Get values from open sales order
    	var osoItem = openSORecord.getSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId:'custcol_bwp_oso_so_item',
	    	line: osoLine
		});
    	
    	var osoLineNumber = openSORecord.getSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId:'line',
	    	line: osoLine
		});
	     
	    var osoDefaultQuantity = openSORecord.getSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId:'custcol_bwp_oso_def_ord_qty',
	    	line: osoLine
		});
	    
	    var osoReservedQuantity = openSORecord.getSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId:'custcol_bwp_oso_reserved_quantity',
	    	line: osoLine
		});
	    
	    var osoOrderedQuantity = openSORecord.getSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId:'custcol_bwp_oso_ord_quantity',
	    	line: osoLine
		});
	    
	    var osoQuantity = openSORecord.getSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId:'quantity',
	    	line: osoLine
		});
	    
	    var osoUnits = openSORecord.getSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId:'units',
	    	line: osoLine
		});
	    
	    var osoRate = openSORecord.getSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId:'rate',
	    	line: osoLine
		});
	    
	    var osoPriceLevel = openSORecord.getSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId:'price',
	    	line: osoLine
		});
	    
	    var osoOpenQuantity = osoQuantity - osoOrderedQuantity - osoReservedQuantity;
//	    var osoOpenQuantity = openSORecord.getSublistValue({ 
//	    	sublistId: 'item', 
//	    	fieldId:'custcol_bwp_oso_open_quantity',
//	    	line: osoLine
//		});	    
	   
	    if (osoDefaultQuantity == 0 ) {
	    	continue;
	    }
	    
	    if (osoOpenQuantity <= 0 ) {
	    	continue;
	    } 

	    if (osoOpenQuantity && osoOpenQuantity < osoDefaultQuantity) {
	    	osoDefaultQuantity = osoOpenQuantity;
	    }
	    
  	    soRecord.selectNewLine({ sublistId: 'item' });

	    soRecord.setCurrentSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId: 'item', 
	    	value: osoItem,
	    	fireSlavingSync: true
		});
	    
	    soRecord.setCurrentSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId: 'price', 
	    	value: osoPriceLevel,
	    	fireSlavingSync: true
		});
	    
	    soRecord.setCurrentSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId: 'quantity', 
	    	value: osoDefaultQuantity,
	    	fireSlavingSync: true
		});
	    
	    soRecord.setCurrentSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId: 'units', 
	    	value: osoUnits,
	    	fireSlavingSync: true
		});
	    
	    soRecord.setCurrentSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId: 'rate', 
	    	value: osoRate,
	    	fireSlavingSync: true
		});
	    
	    soRecord.setCurrentSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId: 'custcol_bwp_open_so_line_number', 
	    	value: osoLineNumber,
	    	fireSlavingSync: true
		});
	    
	    soRecord.commitLine({
	    	sublistId: 'item'
	    });
    }
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}