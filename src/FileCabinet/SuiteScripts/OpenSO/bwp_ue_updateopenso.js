var RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE,UISERVERWIDGETMODULE, URLMODULE, ERRORMODULE;


/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url', 'N/error'], runUserEvent);

function runUserEvent(record, runtime, search, uiserverWidget, url, error) {
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	UISERVERWIDGETMODULE= uiserverWidget;
	URLMODULE= url;
	ERRORMODULE= error;
		
	var returnObj = {};
//	returnObj['beforeLoad'] = _beforeLoad;
	returnObj['beforeSubmit'] = _beforeSubmit;
//	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	
	
}

/**
 * Function definition to be triggered before record is submitted.
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('beforeSubmit started');
	
	
//	logVar('context', scriptContext.type);
	
	switch (scriptContext.type) {
		case scriptContext.UserEventType.CANCEL:
			updateOpenSalesOrder(scriptContext, 'CANCEL');
			break;
		case scriptContext.UserEventType.CREATE:
		case scriptContext.UserEventType.EDIT:
			updateOpenSalesOrder(scriptContext, 'SAVE');
			break;
		case scriptContext.UserEventType.APPROVE:
			updateOpenSalesOrder(scriptContext, 'APPROVE');
			break;
	}
	
	log.debug('beforeSubmit done');
}

/**
 * Function definition to be triggered after record is submitted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {	
	log.debug({
		title: 'afterSubmit started'
	});
	
	log.debug({
		title: 'afterSubmit done'
	});
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}


function updateOpenSalesOrder(scriptContext, action) {
	
//	quantity = Open sales order Quantity
//	custcol_bwp_oso_def_ord_qty = Default Order Quantity ( Setup this quantity when sales order is created from open so )
//	custcol_bwp_oso_ord_quantity = Ordered quantity ( Sales Order from this Open Sales order that have been validated )
//	custcol_bwp_oso_reserved_quantity = Reserved quantity ( Pending Fulfillment sales order )
//	custcol_bwp_oso_open_quantity ( Quantity remain in the open sales order )
	
	var osoTranStatus = 'C';
	var forceOsoTranStatusExpired = false;
	
	var openSOId = scriptContext.newRecord.getValue('custbody_bwp_so_associated_oso');
	
	if (!openSOId) {
		return;
	}
	
	else  {
		
		// Load Open sales order record
		var openSORecord = RECORDMODULE.load({
			type: 'customsale_bwp_open_sales_order',
			id: openSOId,
			isDynamic: true			
		});
		
//		Load Actual sales order
		var soRecord = scriptContext.newRecord;
		
		var soOrderStatus = soRecord.getValue({ 
			fieldId:'orderstatus'
	    });
			    
		// We go through all the new sales order lines
		var maxSOLine = scriptContext.newRecord.getLineCount({
			sublistId: 'item'
		});
		
//		---------------------------------
//		Start FOR
//		---------------------------------		
	    for (var soLine = 0 ; soLine < maxSOLine ; soLine++){
//	    	logVar('NEW LINE', soLine);
    	    var soItem = soRecord.getSublistValue({ 
    	    	sublistId: 'item', 
    	    	fieldId:'item',
    	    	line: soLine
    	    });
    	    
    	    var soQuantity = soRecord.getSublistValue({ 
    	    	sublistId: 'item', 
    	    	fieldId:'quantity',
    	    	line: soLine
    	    });
    	    
    	    var soOrderedQuantity = soRecord.getSublistValue({ 
    	    	sublistId: 'item', 
    	    	fieldId:'custcol_bwp_oso_ord_quantity',
    	    	line: soLine
    	    });
    	    
    	    var soReservedQuantity = soRecord.getSublistValue({ 
    	    	sublistId: 'item', 
    	    	fieldId:'custcol_bwp_oso_reserved_quantity',
    	    	line: soLine
    	    });
    	    
    	    
    	    
//			We get the sublist line item of the openSO	    
	    	var indexOSOItem = openSORecord.findSublistLineWithValue({
				sublistId: 'item', 
				fieldId: 'custcol_bwp_oso_so_item', 
				value: soItem
			});
	    	
	    	var osoline = openSORecord.selectLine({
	    		sublistId: 'item',
	    		line: indexOSOItem  
	    	});
	    	
	    	var osoOrderedQuantity = openSORecord.getCurrentSublistValue({
				sublistId: 'item', 
				fieldId: 'custcol_bwp_oso_ord_quantity'
			});
	    	
	    	var osoReservedQuantity = openSORecord.getCurrentSublistValue({
				sublistId: 'item', 
				fieldId: 'custcol_bwp_oso_reserved_quantity',
			});
	    	
	    	var osoOpenQuantity = openSORecord.getCurrentSublistValue({
				sublistId: 'item', 
				fieldId: 'custcol_bwp_oso_open_quantity'
			});
	    	
	    	var osoQuantity = openSORecord.getCurrentSublistValue({
				sublistId: 'item', 
				fieldId: 'quantity'
			});
	    	
//	    	Adjust Open Sales Order Quantity columns with the new Sales Order Quantity
//	    	Status of the sales order affect which values are update
//	    	Case where sales order is pending fulfillment	    	
	    	if (action == 'SAVE' && soOrderStatus == 'A' ){
	    		logVar('soOrderStatus', soOrderStatus);	    	
		    	if (soReservedQuantity != soQuantity){
		    		    		    		
		    		osoReservedQuantity = osoReservedQuantity - soReservedQuantity + soQuantity;
		    		
		    		osoOpenQuantity =  osoQuantity - osoReservedQuantity - osoOrderedQuantity;
		    		
		    		soReservedQuantity = soQuantity;
		      	}		
//		    	if (osoOpenQuantity > 0){
//	    			osoTranStatus = 'B';
//	    		}	
		    }
	    	
	    	if (action == 'SAVE' && soOrderStatus != 'A' && soOrderStatus != 'C' ){
	    		logVar('soOrderStatus', soOrderStatus);	    	
		    	if (soReservedQuantity != soQuantity){
		    		    		    		
		    		osoReservedQuantity = osoReservedQuantity - soReservedQuantity + soQuantity;
		    		
		    		osoOpenQuantity =  osoQuantity - osoReservedQuantity - osoOrderedQuantity;
		    		
		    		soReservedQuantity = soQuantity;
		    		
		    	}	
//		    	if (osoOpenQuantity > 0){
//	    			osoTranStatus = 'B';
//	    		}	  	    			    
		    }
	    	
//	    	Case where sales order status is changed
	    	if (action == 'APPROVE'){
	    		
	    		osoReservedQuantity = osoReservedQuantity - soReservedQuantity;
	    		osoOrderedQuantity = osoOrderedQuantity - soOrderedQuantity + soQuantity;
	    		
	    		osoOpenQuantity =  osoQuantity - osoReservedQuantity - osoOrderedQuantity;
	    		
	    		soOrderedQuantity = soQuantity; 
	    		soReservedQuantity = 0;
	    		
//	    		if (osoOpenQuantity > 0){
//	    			osoTranStatus = 'B';
//	    		}	    		
	    	}
	    	
//	    	Case where sales order is cancelled 
	    	if (action == 'CANCEL'){
//	    		logVar('soOrderStatus', soOrderStatus);
	    		osoReservedQuantity = osoReservedQuantity - soReservedQuantity ;
	    		osoOrderedQuantity = osoOrderedQuantity - soOrderedQuantity;
	    		
	    		osoOpenQuantity =  osoQuantity - osoReservedQuantity - osoOrderedQuantity;
	    		
	    		soReservedQuantity = 0;
	    		soOrderedQuantity = 0;
	    		
//	    		osoTranStatus = 'B';	    		
	    	}

//	    	Update open sales order different value from the item sublist 
	    	openSORecord.setCurrentSublistValue({
				sublistId: 'item', 
				fieldId: 'custcol_bwp_oso_ord_quantity',
				value : osoOrderedQuantity
			});
	    	
	    	openSORecord.setCurrentSublistValue({
				sublistId: 'item', 
				fieldId: 'custcol_bwp_oso_reserved_quantity',
				value : osoReservedQuantity
			});
	    	
	    	openSORecord.setCurrentSublistValue({
				sublistId: 'item', 
				fieldId: 'custcol_bwp_oso_open_quantity',
				value : osoOpenQuantity
	    	});
	    	
	    	openSORecord.commitLine({
				sublistId: 'item',
				fieldId: 'custcol_bwp_oso_reserved_quantity',
    	    	line: osoline
			});
	    	
	    	
	    	
//	    	Update sales order reserved and ordered quantity
	    	soRecord.setSublistValue({ 
    	    	sublistId: 'item', 
    	    	fieldId:'custcol_bwp_oso_ord_quantity',
    	    	value: soOrderedQuantity,
    	    	line: soLine
    	    });
//	    	logVar('soOrderedQuantity', soOrderedQuantity);
	    	soRecord.setSublistValue({ 
    	    	sublistId: 'item', 
    	    	fieldId:'custcol_bwp_oso_reserved_quantity',
    	    	value: soReservedQuantity,
    	    	line: soLine
    	    });
//	    	logVar('soReservedQuantity', soReservedQuantity);
	    	
    	}; //end for
    	osoTranStatus= checkOpenSalesOrderOpenQuantity(openSORecord);
    	openSORecord.setValue({
    		fieldId: 'transtatus',
    		value: osoTranStatus
    	});
    	openSORecord.save();
    	
    };
}


function checkOpenSalesOrderOpenQuantity(openSORecord){
	var osoTranStatus = 'C';
	var counterOpenSalesOrderLines = openSORecord.getLineCount('item');
	
	for (var i = 0; i < counterOpenSalesOrderLines; i++){
		openSORecord.selectLine({
			sublistId: 'item',
			line: i
		});			
		var osoOpenQuantity = openSORecord.getCurrentSublistValue({
			sublistId:'item',
			fieldId: 'custcol_bwp_oso_open_quantity'
		});		
		if (osoOpenQuantity > 0){
			osoTranStatus = 'B';
		}		
	}	
	return osoTranStatus;		
}