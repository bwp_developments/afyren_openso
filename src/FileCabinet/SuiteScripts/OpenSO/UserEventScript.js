var RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE, TRANSLATIONMODULE, UISERVERWIDGETMODULE, URLMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search', 'N/translation', 'N/ui/serverWidget', 'N/url'], runUserEvent);

function runUserEvent(record, runtime, search, translation, uiserverWidget, url) {
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	TRANSLATIONMODULE= translation;
	UISERVERWIDGETMODULE= uiserverWidget;
	URLMODULE= url;
	
	var returnObj = {};
	returnObj['beforeLoad'] = _beforeLoad;
//	returnObj['beforeSubmit'] = _beforeSubmit;
	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug({
		title: 'beforeLoad started'
	});
	
	var woRecord = scriptContext.newRecord;
	var woRecordId = woRecord.getValue({
		fieldId: 'id'
	});
	
	// 15/04/2020 - Enhancement for quality module - Begin
	// Get setup on user's subsidiary
	var subsidiary = woRecord.getValue('subsidiary');
	var qualitySetup = QUALITYMODULE.getQualitySetup(subsidiary);
	if (subsidiary) {
		customizeLayoutForOutgoingQuality(scriptContext, qualitySetup);			
	}
	// 15/04/2020 - Enhancement for quality module - End
	
	if (scriptContext.type == scriptContext.UserEventType.VIEW) {
		// Attach the client script to handle actions on buttons
    	scriptContext.form.clientScriptModulePath = 'SuiteScripts/QualityModule/mob_cs_workorderquality.js';
		
		var userLanguage = RUNTIMEMODULE.getCurrentUser().getPreference('language');
		var printButtonLabel;
		switch(userLanguage){
			case 'fr_CA':
    		case 'fr_FR':
    			printButtonLabel = 'Etiquette Production';
    			break;
			default: 
				printButtonLabel = 'Print Labels';
		}
		/*
    	//get the url for the suitelet - add woRecordId parameter
    	var suiteletURL = URLMODULE.resolveScript({
    	 'scriptId':'customscript_mob_sl_display_prod_labels',
    	 'deploymentId':'customdeploy_mob_sl_display_prod_labels',
    	 'returnExternalUrl': false
    	}) + '&mob_worecordid=' + woRecordId;

    	//hook point for the button that will be fired client side;
//    	var scr = "require([], function() { window.open('"+creURL+"','_blank');});";
    	var scr = "require([], function() { window.open('"+suiteletURL+"');});";

    	// log.debug(funcName, scr);
    	*/

    	//add button to the form; when clicked, call the scr function 
		scriptContext.form.addButton({
			id : 'custpage_mob_button_print_labels',
			label : printButtonLabel,
			functionName : 'printLabels'
		});
		
		// Disable close button until quality results are completed
		// Display 'Force Close' custom button
		if (qualitySetup.outgoingQualityFlag) {
			var forceCloseParam = scriptContext.request.parameters.custparam_mob_forceclose;
			if (!forceCloseParam) {
				var closeButton = scriptContext.form.getButton({
					id: 'closeremaining' //'close' 'Close'
				});
				if (closeButton && !checkQualityTestAreCompleted(woRecordId)) {
					closeButton.isDisabled = true;
					
			    	//get the url for the suitelet - add woRecordId parameter
			    	var woRecordURL = URLMODULE.resolveRecord({
			    		recordType: 'workorder',
			    		recordId: woRecordId,
			    		params: {custparam_mob_forceclose: true}
			    	});
			    	
			    	//hook point for the button that will be fired client side;
//			    	var scr = "require([], function() { window.open('"+woRecordURL+"');});";
//			    	var scr = "require([], function() { window.location = '"+woRecordURL+"';});";
			    	
					var forceCloseButton = scriptContext.form.addButton({
						id: 'custpage_mob_forceclosebutton',
						label: TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'FORCE_CLOSE_BTN'})(),
						functionName: 'openWithForceClose'
					});				
				}				
			}
		}
		
		// Add Receive button when outgoing quality is enabled for the subsidiary
		// Check status == Released ('B') or In Process ('D')
		var orderStatus = woRecord.getValue('orderstatus');
		log.debug('order status ' + orderStatus);
		if (qualitySetup.outgoingQualityFlag && ['B', 'D'].indexOf(orderStatus) >= 0 ) {
			// At beforeLoad stage related lists do not contain any data ==> lineCount is always zero
			// ==> need to perform a search action on customrecord_mob_wo_pre_completion record
//			var lineCount = woRecord.getLineCount({
//				sublistId: 'recmachcustrecord_mob_cmpl_wonumber'
//			});
			var searchObj = SEARCHMODULE.create({
				type: 'customrecord_mob_wo_pre_completion',
				columns: ['internalid'],
				filters: [SEARCHMODULE.createFilter({
					name: 'custrecord_mob_cmpl_wonumber',
					operator: SEARCHMODULE.Operator.IS,
					values: woRecordId
				})]
			});
			var lineCount = searchObj.run().getRange({start: 0, end: 1}).length;
			if (lineCount > 0) {
		    	//add button to the form; when clicked, call the scr function 
				scriptContext.form.addButton({
					id : 'custpage_mob_button_receive',
					label : TRANSLATIONMODULE.get({collection: 'custcollection_mob_outgoingquality', key: 'RECEIVE_BTN'})(),
					functionName : 'receive'
				});				
			}
		}
	}
	
	log.debug({
		title: 'beforeLoad done'
	});
}

/**
 * Function definition to be triggered before record is submitted.
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
//	var woRecord = scriptContext.newRecord;
//	var woRecordId = woRecord.getValue({
//		fieldId: 'id'
//	});
//	// Load the saved search and add a filter on the internalId of the work order
//	var searchObj = search.load({
//		id: 'customsearch_mob_etiquettes_production'
//	});
//	var searchId = searchObj.searchId;
//	var filterOnInternalId = search.createFilter({
//		name: 'internalid',
//		operator: search.Operator.ANYOF,
//		values: woRecordId
//	});
//	var filters = searchObj.filters.push(filterOnInternalId);
//	
//	// Open the result of the filter
//	redirect.toSearchResult({
//		search: searchObj
//	});    			
}

/**
 * Function definition to be triggered after record is submitted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {	
	log.debug({
		title: 'Start afterSubmit Script'
	});
	
	var scriptObj = RUNTIMEMODULE.getCurrentScript();
	log.debug({
		title: 'Remaining Usage',
		details: scriptObj.getRemainingUsage()
	});
	
	var indexLength = INDEXLENGTH; 			// The length of the index part of package numbers
	var indexSeparator = INDEXSEPARATOR;	// The separator to use between the prefix part and the index part of the package number	
	
	//var packageSize = 0; 			// The size of a package
	//var numberOfSilos = 1;
	//var numberOfContainers = 0; 	// The number of containers (bigbag, octabin, ...) - can be calculated or populated by the user on the WO
	//var containersPerSilo = 0;

	var createPreCompletionList = false;
	
	var woRecord = scriptContext.newRecord;
	var woRecordId = woRecord.getValue('id');
	
	// Get quality setup on user's subsidiary
	var qualitySetup = QUALITYMODULE.getQualitySetup();
	
	// Read information on the WO
	var numberOfPackaging = woRecord.getValue({
		fieldId: 'custbodyrvd_number_of_packaging'
	});
	
	var item = woRecord.getValue({
		fieldId: 'assemblyitem'
	});
	var woQuantity = woRecord.getValue({
		fieldId: 'quantity'
	});
	var subsidiary = woRecord.getValue({
		fieldId: 'subsidiary'
	});
	var automaticLotNumbering = false;
	if (subsidiary) {
		var fieldsValues = SEARCHMODULE.lookupFields({
			type: SEARCHMODULE.Type.SUBSIDIARY,
			id: subsidiary,
			columns: ['custrecord_wo_lotnumber_automatic']
		});
		logRecord(fieldsValues, 'fieldsValues');
		if ('custrecord_wo_lotnumber_automatic' in fieldsValues) {
			automaticLotNumbering = fieldsValues['custrecord_wo_lotnumber_automatic'];
		}	
	}
	var lotNumber = woRecord.getValue({
		fieldId: 'custbody_mob_wo_lot'
	});
	if (!lotNumber && automaticLotNumbering) {
		var fieldsValues = SEARCHMODULE.lookupFields({
			type: SEARCHMODULE.Type.WORK_ORDER,
			id: woRecordId,
			columns: ['tranid']
		});
		if ('tranid' in fieldsValues) {
//			lotNumber = fieldsValues['tranid'].slice(4);
			lotNumber = fieldsValues['tranid'];
			RECORDMODULE.submitFields({
				type: RECORDMODULE.Type.WORK_ORDER,
				id: woRecordId,
				values: {
					custbody_mob_wo_lot: lotNumber
				}
			});
		}
	}
	var capacityOfSilo = woRecord.getValue({
		fieldId: 'custbody_mob_silocapacity'
	});
	var capacityOfContainer = woRecord.getValue({
		fieldId: 'custbody_mob_containercapacity'
	});
	
	var preCompletionSublist = woRecord.getSublist({
		sublistId: 'recmachcustrecord_mob_cmpl_wonumber'
	});
	
	var completionListLineCount = woRecord.getLineCount({
        sublistId : 'recmachcustrecord_mob_cmpl_wonumber'
    });	
	
	if (completionListLineCount == 0) {
		createPreCompletionList = true;
	}
	
	// When editing the WO, check for change in information that have an impact on WO labels
	// In case some value has changed, delete the existing wo_pre_completion records
	if (scriptContext.type == scriptContext.UserEventType.EDIT){
    	var woRecordOld = scriptContext.oldRecord;
		var woQuantityOld = woRecordOld.getValue({
    		fieldId: 'quantity'
    	});
		var lotNumberOld = woRecordOld.getValue({
    		fieldId: 'custbody_mob_wo_lot'
    	});
		var numberOfPackagingOld = woRecordOld.getValue({
			fieldId: 'custbodyrvd_number_of_packaging'
		});
		
		// 15/04/2020 - Enhancement for quality module - Begin
		var capacityOfSiloOld = woRecordOld.getValue({
    		fieldId: 'custbody_mob_silocapacity'
    	});
		var capacityOfContainerOld = woRecordOld.getValue({
    		fieldId: 'custbody_mob_containercapacity'
    	});
		// 15/04/2020 - Enhancement for quality module - End
		
		// Remove existing wo pre completion and production silo
		if (woQuantity != woQuantityOld || lotNumber != lotNumberOld || numberOfPackaging != numberOfPackagingOld
				|| capacityOfSilo != capacityOfSiloOld || capacityOfContainer != capacityOfContainerOld) {
			createPreCompletionList = true;
			/*
			 * This code no longer works because it is in aftersubmit. Initially it was in before submit
			if (completionListLineCount > 0) {
				for (var i = 1 ; i <= completionListLineCount ; i++) {
					woRecord.removeLine({
						sublistId: 'recmachcustrecord_mob_cmpl_wonumber',
						line: 0,
						ignoreRecalc: true
					});
				}	
				var siloListLineCount = woRecord.getLineCount({
			        sublistId : 'recmachcustrecord_mob_cmpl_silowonumber'
			    });
				log.debug({
					title: 'siloListLineCount',
					details: siloListLineCount
				});
				for (var i = 1 ; i <= siloListLineCount ; i++) {
					woRecord.removeLine({
						sublistId: 'recmachcustrecord_mob_cmpl_silowonumber',
						line: 0,
						ignoreRecalc: true
					});
				}
			}
			*/
			
			deleteWoPreCompletions(woRecordId);
		}	    	
	}
	
	if (!qualitySetup.outgoingQualityFlag) {
		// Original functionality (without outgoing quality)
		var numberOfContainers = parseInt(numberOfPackaging || 0, 10);
		capacityOfSilo = woQuantity;
		// In case number of containers is not populated on the WO, calculate the value based on container capacity parameter
		if (numberOfContainers <= 0) {
			var scriptObj = RUNTIMEMODULE.getCurrentScript();
			// When quantity exceeds the capacity of a container it must be dispatched on several containers
			capacityOfContainer = scriptObj.getParameter({
				name: 'custscript_mob_lotsize'
			})
			// Calculate the number of complete packages
			// https://stackoverflow.com/questions/4228356/integer-division-with-remainder-in-javascript
			numberOfContainers = Math.floor(woQuantity/capacityOfContainer);
			// Calculate the size of the last package
			var lastContainerSize = woQuantity % capacityOfContainer;
			if (lastContainerSize > 0) {
				numberOfContainers++;
			} else {
				lastContainerSize = capacityOfContainer;
			}
		} else {
			capacityOfContainer = Math.ceil(woQuantity/numberOfContainers);
		}	
	} else {	
		if (!qualitySetup.multiSiloFlag) {
			// Multisilo mode is not activated: default the capacity of silo with the total quantity of the wo
			capacityOfSilo = woQuantity;
		}
	}
	
	if (scriptContext.type == scriptContext.UserEventType.CREATE && preCompletionSublist != null){
		/*
        // Use event type EDIT for testing
        if (scriptContext.type == scriptContext.UserEventType.EDIT){
		*/
		createPreCompletionList = true;
	}
	
	if (createPreCompletionList) {		
		createPrecompletionList(woRecord, item, woQuantity, capacityOfSilo, capacityOfContainer, lotNumber);	
	}
	
	log.debug({
		title: 'End afterSubmit Script'
	});
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}
