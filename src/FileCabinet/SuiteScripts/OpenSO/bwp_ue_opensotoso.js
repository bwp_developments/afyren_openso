var RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE,UISERVERWIDGETMODULE, URLMODULE;


/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url'], runUserEvent);

function runUserEvent(record, runtime, search, uiserverWidget, url) {
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	UISERVERWIDGETMODULE= uiserverWidget;
	URLMODULE= url;
		
	var returnObj = {};
	returnObj['beforeLoad'] = _beforeLoad;
	returnObj['beforeSubmit'] = _beforeSubmit;
//	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('beforeLoad started');
	
	// Attach the client script to handle actions on buttons
	scriptContext.form.clientScriptModulePath = 'SuiteScripts/OpenSO/bwp_cs_opensotoso.js';
	
	
//	//Create button only if status and date match
//	A - wait approval
//	B - in process
//	H - closed
	var showButton = false;
	var openQuantityFound = false;
	var osoRecord = scriptContext.newRecord;
	var openSOTranStatus = osoRecord.getValue('transtatus');
	var openSOStartDate = osoRecord.getValue('startdate');
	var openSOEndDate = osoRecord.getValue('enddate');

	var dateToday =  new Date();
	
	var maxOSOLine = osoRecord.getLineCount({
	    	sublistId: 'item'
	    });
	for (var osoLine = 0 ; osoLine < maxOSOLine ; osoLine++){ 
		var osoOpenQuantity = osoRecord.getSublistValue({ 
	    	sublistId: 'item', 
	    	fieldId:'custcol_bwp_oso_open_quantity',
	    	line: osoLine
		});
		
		if (osoOpenQuantity > 0){
			openQuantityFound = true;
		}		
	}
	
	if (openSOTranStatus == 'B'){
		showButton = true;
	}
	
	if (showButton &&  openQuantityFound){
//	add button to the form; when clicked, call the scr function 
		scriptContext.form.addButton({
			id : 'custpage_bwp_button_create_so',
			label : 'Create SO',
			functionName : 'createSO'
		});
    };
	
	log.debug('beforeLoad done');
}

/**
 * Function definition to be triggered before record is submitted.
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug({
		title: 'beforeSubmit started'
	});
	
//	logVar("Context",scriptContext.type);
	
	
	switch (scriptContext.type) {
		case scriptContext.UserEventType.CREATE :
			updateCustomFieldValues(scriptContext,'CREATE');
			break;
		case scriptContext.UserEventType.EDIT :
			updateCustomFieldValues(scriptContext,'EDIT');
			break;
	
	}
	
	log.debug({
		title: 'beforeSubmit done'
	});	
}

/**
 * Function definition to be triggered after record is submitted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {	
	log.debug({
		title: 'afterSubmit started'
	});
	
	log.debug({
		title: 'afterSubmit done'
	});
}

function updateCustomFieldValues(scriptContext, callType){
	
//	Load Actual sales order
	var osoRecord = scriptContext.newRecord;
	
	// We go through all the new sales order lines
	var maxOSOLine = osoRecord.getLineCount({
		sublistId: 'item'
	});
	
	
	switch (callType) {
		
		case 'CREATE':		
//			---------------------------------
//			Start FOR
//			---------------------------------		
		    for (var osoLine = 0 ; osoLine < maxOSOLine ; osoLine++){
		    	
		    	osoRecord.setSublistValue({
					sublistId: 'item', 
					fieldId: 'custcol_bwp_oso_ord_quantity',
					line: osoLine,
					value : '0'
				});
		    	
		    	osoRecord.setSublistValue({
					sublistId: 'item', 
					fieldId: 'custcol_bwp_oso_reserved_quantity',
					line: osoLine,
					value : '0'
				});
		    	
		    	var osoQuantity = osoRecord.getSublistValue({
					sublistId: 'item', 
					fieldId: 'quantity',
					line: osoLine
				});
		    	
		    	osoRecord.setSublistValue({
					sublistId: 'item', 
					fieldId: 'custcol_bwp_oso_open_quantity',
					line: osoLine,
					value : osoQuantity
				});		
		    	
		    	osoRecord.commitLine;
		    }	
//			---------------------------------
//			End FOR
//			---------------------------------	
		    break;
		case 'EDIT':
//			---------------------------------
//			Start FOR
//			---------------------------------		
		    for (var osoLine = 0 ; osoLine < maxOSOLine ; osoLine++){
		    	
		    	var osoOrderedQuantity = osoRecord.getSublistValue({
					sublistId: 'item', 
					fieldId: 'custcol_bwp_oso_ord_quantity',
					line: osoLine
				});
		    	
		    	var osoReservedQuantity = osoRecord.getSublistValue({
					sublistId: 'item', 
					fieldId: 'custcol_bwp_oso_reserved_quantity',
					line: osoLine
				});
		    	
		    	var osoQuantity = osoRecord.getSublistValue({
					sublistId: 'item', 
					fieldId: 'quantity',
					line: osoLine
				});
		    	
		    	var osoOpenQuantity = osoQuantity - osoOrderedQuantity - osoReservedQuantity;
		    	
		    	osoRecord.setSublistValue({
					sublistId: 'item', 
					fieldId: 'custcol_bwp_oso_open_quantity',
					line: osoLine,
					value : osoOpenQuantity
				});		
		    	
		    	osoRecord.commitLine;
		    }	
//			---------------------------------
//			End FOR
//			---------------------------------
		    break;		
	}	
	osoRecord.save;	
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}
